import { TestBed, inject } from '@angular/core/testing';

import { AnimalManagerService } from './animal-manager.service';

describe('AnimalManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnimalManagerService]
    });
  });

  it('should be created', inject([AnimalManagerService], (service: AnimalManagerService) => {
    expect(service).toBeTruthy();
  }));
});
