# Application Save SPA

Step 1)
-------

**Clone the repository**

On the terminal : `git clone https://gitlab.com/LionSCP/save-spa.git`

Step 2)
-------

**Install the npm**

On each folder `./save-spa/api` and `./save-spa/web-saveSpa` 

run `npm install` on your terminal

Step 3)
-------

You can configure URLs on the file environment.ts : `./save-spa/web-saveSpa/src/environments/environment.ts`

You can set an IP adress. 

If you want to run application on localhost, replace :

<pre><code>
export const environment = {
  production: false,
  url: "http://localhost/api/",
  web_url: "http://localhost:4200"
};
</code></pre>

Step 4)
-------

**Move to ./save-spa/api**

Start API server from terminal : `node app.js`

(If you have nodemon use `nodemon app.js` or install it with `npm install --save nodemon`)

Require Node.js

**Move to ./save-spa/web-saveSpa/**

Start Web Client from terminal : `ng serve`

Require Angular CLI

Step 5)
-------

Test it at `http://localhost:4200/` and enjoy :)

Database
--------

MongoDB with MLab at `https://mlab.com/`

Email sender
-----------

**Be careful with your credentials**

Replace this informations with your email credentials at `./save-spa/api/config.js`

<pre><code>
const email = {
    id: 'example@gmail.com',
    password: '*******'
}
</code></pre>

Check your email account configuration at `./save-spa/api/controllers/alertsController.js`

<pre><code>
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: config.email.id, 
        pass: config.email.password
    }
});
</code></pre>

## Enjoy





