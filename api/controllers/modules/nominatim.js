const axios = require('axios');

nominatimURL = 'https://nominatim.openstreetmap.org/search/?q=';
optionsURL = '&format=json&addressdetails=1';
reverseURL = 'https://nominatim.openstreetmap.org/reverse?'

const getGeocode = async (location) => {
    let url = nominatimURL + location + optionsURL;

    try {
        let res = await axios.get(url);
        res = checkData(res.data);
        return res;
    } catch (error) {
        console.error("getGeocode :", error);
    }
}

const checkData = (data) => {
    if (data.length == 0) return null
    else if (data.length == 1) {
        if (data[0].address.country != "France") return null
        else {
            return data
        }
    } else {
        data.forEach(element => {
            if (element.addres.country == "France") return [element];
            else return null
        });
    }
}

module.exports = {
    getGeocode,
}
