import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SquadManagerService {

    /**
   * Base URL of API
   * You can find this on environment.ts 
   */
  private _url: string;

  constructor(
    private http: HttpClient
  ) { 
    this._url = environment.url;
  }

  /**
   * Retrieve all squads from database
   * GET method
   */
  getSquads() {
    return this.http.get(this._url + "squads");
  }
}
