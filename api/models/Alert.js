const mongoose = require('mongoose');
const validator = require('validator');
const nominatim = require('../controllers/modules/nominatim');

/**
 * Alert schema to model database
 */
const AlertSchema = mongoose.Schema({
    date : {
        type: Date,
        required: true
    }, 
    time_slot: {
        type: {
            start: Number,
            end: Number
        },
        required: true,
        validate: {
            validator: (v) => {
                if (v.start < v.end) return true;
                else return false;
            },
            message: "{VALUE} is not a valid time slot"
        }
    },
    alertor: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    adress: {
        type: {
            number: Number,
            street: String,
            zipcode: Number,
            city: String,
        },
        required: true
    },
    animal: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Animal',
        required: true
    },
    health: {
        type: Number,
        enum: [1, 2, 3, 4],
        required: true
    },
    color: {
        type: String,
        required: true,
    },
    choker: {
        type: Boolean,
        required: true
    },
    squad: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Squad',
        default: null
    },
    status: {
        type: Number,
        enum: [0, 1, 2, 3, 4],
        default: 0
    },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            default: null
        },
    }

});

AlertSchema.methods.generatePoint = function () {
    let alert = this;
    let place = this.adress.number + ' ' + this.adress.street + ',' + this.adress.zipcode + ',' + this.adress.city;

    nominatim.getGeocode(place).then((latLng) => {
        if (latLng != null) {
            alert.location = {
                type: "Point",
                coordinates: [latLng[0].lat, latLng[0].lon]
            };
        } else {
            alert.location = {
                type: "Point",
                coordinates: null
            };
        }

        return alert.save().then(() => {
            return alert ;
        });

    })

}

module.exports = mongoose.model('Alert', AlertSchema) 