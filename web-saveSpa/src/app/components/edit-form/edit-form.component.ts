import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, FormBuilder } from "@angular/forms";
import { AnimalManagerService } from 'src/app/services/animal-manager.service';
import { SquadManagerService } from 'src/app/services/squad-manager.service';
import { AlertsManagerService } from 'src/app/services/alerts-manager.service';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {

  /**
   * List of squads from database
   */
  private squads: any;

  /**
   * List of animals from database
   */
  private animals: any;

  /**
   * Reactive form to perform alert's update
   */
  private alertForm: any;

  /**
   * Current alert on update
   */
  private alert: any;

  /**
   * Alert's ID from url params
   */
  private alertID: string;

  constructor(
    private animalManager: AnimalManagerService,
    private squadManager: SquadManagerService,
    private alertManager: AlertsManagerService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(
      params => this.alertID = params.id
    );

    this.alertForm = this.fb.group({
      alertor: new FormControl([''], Validators.compose([Validators.email])),
      adressNumber: new FormControl([''], Validators.compose([Validators.min(1)])),
      adressStreet: new FormControl([''], Validators.compose([Validators.minLength(10)])),
      adressZipcode: new FormControl([''], Validators.compose([Validators.min(1), Validators.max(99000)])),
      adressCity: new FormControl([''], Validators.compose([Validators.minLength(3)])),
      health: new FormControl(['']),
      color: new FormControl([''], Validators.compose([Validators.minLength(4)])),
      choker: new FormControl(['']),
      status: new FormControl(['']),
      squad: new FormControl([''])
    })
  }

  ngOnInit() {
    this.animalManager.getAnimals()
      .subscribe((data) => {
        this.animals = data;
      });

    this.squadManager.getSquads()
      .subscribe((data) => {
        console.log(data)
        this.squads = data;
      })

    this.alertManager.getAlertById(this.alertID)
      .subscribe((data) => {
        this.alert = data[0];
      });

  }

  /**
   * Function to send data toward server to perform alert's update 
   */
  sendAlert() {

    let data = this.alert;

    if (this.alert.squad === null && this.alert.status === 0 && this.alertForm.value.squad[0] !== "") {
      data.squad = this.alertForm.value.squad;
      data.status = 1;
    } else if (this.alert.squad !== null && this.alertForm.value.status[0] !== "") {
      data.status = Number(this.alertForm.value.status);
    }

    if (this.alertForm.value.adressNumber[0] !== "") data.adress.number    = this.alertForm.value.adressNumber;
    if (this.alertForm.value.adressStreet[0] !== "") data.adress.street    = this.alertForm.value.adressStreet;
    if (this.alertForm.value.adressZipcode[0] !== "") data.adress.zipcode  = this.alertForm.value.adressZipcode;
    if (this.alertForm.value.adressCity[0] !== "") data.adress.city        = this.alertForm.value.adressCity;
    if (this.alertForm.value.alertor[0] !== "") data.alertor               = this.alertForm.value.alertor;
    if (this.alertForm.value.choker[0] !== "") data.choker                 = this.alertForm.value.choker;
    if (this.alertForm.value.color[0] !== "") data.color                   = this.alertForm.value.color;
    if (this.alertForm.value.health[0] !== "") data.health                 = this.alertForm.value.health;

  this.alertManager.updateAlert(this.alertID, data)
  .subscribe(res => {
    console.log(res);
  }, error => {
    console.log(error);
  })

  this.router.navigateByUrl('/dashboard');
}
}
