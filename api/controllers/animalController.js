const Animal = require('../models/Animal');

/**
 * Function to retrieve all animals from database
 * @param {*} req 
 * @param {*} res 
 */
const getAnimals = async (req, res) => {

    try {
        let animals = await Animal.find();
        res.status(200).send(animals);
    } catch (err) {
        res.sendStatus(500);
        console.error("An error was occured when try to retrieve animals from database");
    }
}

/**
 * Function to add an animal to database
 * Used only by developers
 * @param {*} req 
 * @param {*} res 
 */
const addAnimal = async (req, res) => {
    const body = req.body;

    const newAnimal = new Animal(body);
    try {
        let animal = await newAnimal.save();
        res.status(201).send(animal);
        
    } catch (err) {
        res.sendStatus(500);
        console.error("An error was occured when to try add animal to database");
    }
}

module.exports = {
    addAnimal,
    getAnimals
}