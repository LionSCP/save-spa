import { TestBed, inject } from '@angular/core/testing';

import { AlertsManagerService } from './alerts-manager.service';

describe('AlertsManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertsManagerService]
    });
  });

  it('should be created', inject([AlertsManagerService], (service: AlertsManagerService) => {
    expect(service).toBeTruthy();
  }));
});
