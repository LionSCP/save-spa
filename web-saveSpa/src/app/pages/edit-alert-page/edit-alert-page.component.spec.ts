import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAlertPageComponent } from './edit-alert-page.component';

describe('EditAlertPageComponent', () => {
  let component: EditAlertPageComponent;
  let fixture: ComponentFixture<EditAlertPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAlertPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAlertPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
