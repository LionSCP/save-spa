import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAlertPageComponent } from './add-alert-page.component';

describe('AddAlertPageComponent', () => {
  let component: AddAlertPageComponent;
  let fixture: ComponentFixture<AddAlertPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAlertPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAlertPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
