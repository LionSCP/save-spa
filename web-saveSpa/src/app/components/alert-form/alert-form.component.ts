import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, FormBuilder } from "@angular/forms";
import { AnimalManagerService } from 'src/app/services/animal-manager.service';
import { SquadManagerService } from 'src/app/services/squad-manager.service';
import { AlertsManagerService } from 'src/app/services/alerts-manager.service';
import { Validators } from '@angular/forms';
import { CustomValidator } from 'src/app/validators/custom-validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alert-form',
  templateUrl: './alert-form.component.html',
  styleUrls: ['./alert-form.component.scss']
})
export class AlertFormComponent implements OnInit {

  /**
   * List of squads from database
   */
  private squads: any;

  /**
   * List of animals from database
   */
  private animals: any;

  /**
   * Reactive form to create an alert
   */
  private alertForm: any;

  constructor(
    private animalManager: AnimalManagerService,
    private squadManager: SquadManagerService,
    private alertManager: AlertsManagerService,
    private fb: FormBuilder,
    private router: Router,
  ) {}

  ngOnInit() {
    this.animalManager.getAnimals()
    .subscribe((data) => {
      this.animals = data;
    });

    this.squadManager.getSquads()
    .subscribe((data) => {
      this.squads = data;
    })
    
    this.alertForm = this.fb.group({
      date: new FormControl([''], Validators.required),
      timeSlot_start: new FormControl([null], Validators.compose([Validators.required, Validators.min(1), Validators.max(24)])), 
      timeSlot_end: new FormControl([null], Validators.compose([Validators.required, Validators.min(1), Validators.max(24)])), 
      alertor: new FormControl([''], Validators.compose([Validators.required, Validators.email])),
      animal: new FormControl([''], Validators.required),
      adressNumber: new FormControl([''], Validators.compose([Validators.required, Validators.min(1)])),
      adressStreet: new FormControl([''], Validators.compose([Validators.required, Validators.minLength(10)])),
      adressZipcode: new FormControl([''], Validators.compose([Validators.required, Validators.min(1), Validators.max(99000)])),
      adressCity: new FormControl([''],Validators.compose([Validators.required, Validators.minLength(3)])),
      health: new FormControl([''],Validators.required),
      color: new FormControl([''],Validators.compose([Validators.required, Validators.minLength(4)])),
      choker: new FormControl(false,Validators.required)
    }, {validator: CustomValidator.timeSlotValidator})
  }

  /**
   * Function to send data to server and create an alert
   */
  sendAlert() {
    let data = {
      adress: {
        number: this.alertForm.value.adressNumber,
        street: this.alertForm.value.adressStreet,
        zipcode: this.alertForm.value.adressZipcode,
        city: this.alertForm.value.adressCity
      },
      alertor: this.alertForm.value.alertor,
      animal: this.alertForm.value.animal,
      choker: this.alertForm.value.choker,
      color: this.alertForm.value.color,
      date : this.alertForm.value.date,
      health: this.alertForm.value.health,
      time_slot: {
        start: this.alertForm.value.timeSlot_start,
        end: this.alertForm.value.timeSlot_end
      }
    }

    this.alertManager.addAlert(data)
    .subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    })

      this.router.navigateByUrl('/dashboard');
  }

}
