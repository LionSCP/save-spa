const Squad = require('../models/Squad');

/**
 * Function to retrieve all squads from database
 * @param {*} req 
 * @param {*} res 
 */
const getSquads = async (req, res) => {

    try {
        let squads = await Squad.find();
        res.status(200).send(squads);
    } catch (err) {
        res.sendStatus(500);
        console.error("An error was occured when try to retrieve squads from database");
    }
}

/**
 * Function to add a squad to database
 * Used only by developers
 * @param {*} req 
 * @param {*} res 
 */
const addSquad = async (req, res) => {

    const body = req.body;

    try {
        const newSquad = new Squad(body);
        let squad = await newSquad.save();
        res.status(200).send(squad);
    } catch (err) {
        res.sendStatus(500);
        console.error("Impossible to add new squad to database");
    }
}

module.exports = {
    getSquads,
    addSquad
}