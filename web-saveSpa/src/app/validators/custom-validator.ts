import { ValidatorFn, AbstractControl, FormControl, FormGroup } from "@angular/forms";

export class CustomValidator {

    /**
     * Customm validator to compare End and Start of Time-slot
     * If Start < End, an error is raised
     * @param group 
     */
    static timeSlotValidator(group: FormGroup) {
        return group.controls.timeSlot_end.value > group.controls.timeSlot_start.value ? null : { timeSlotError : true }
    }

}
