const mongoose = require('mongoose');

/**
 * Squad schema to model database
 */
const SquadSchema = mongoose.Schema({
    name : {
        type: String,
        required: true,
        unique: true
    }
})

module.exports = mongoose.model('Squad', SquadSchema)