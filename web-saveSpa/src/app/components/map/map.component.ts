import { Component, OnInit } from '@angular/core';
import { MapManagerService } from 'src/app/services/map-manager.service';
import { AlertsManagerService } from 'src/app/services/alerts-manager.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  constructor(
    private mapManager: MapManagerService,
    private alertManager: AlertsManagerService
  ) { }

  ngOnInit() {
    this.mapManager.generateMap();

    this.alertManager.getAlerts()
    .subscribe(
      (alerts) => {
        console.log(alerts);
        this.mapManager.putMarkers(alerts);
      },
      error => {console.log(error)}
    );

  }

}
