import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

/**
 * Headers for Http request and CORS
 */
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin':'*',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'Origin, Content-Type'
  })
}

@Injectable({
  providedIn: 'root'
})
export class AlertsManagerService {

  /**
   * Base URL of API
   * You can find this on environment.ts 
   */
  private _url: string;

  constructor(
    private http: HttpClient
  ) {
    this._url = environment.url
   }

  /**
   * Retrieve all alerts from database
   * GET method
   */
   getAlerts() {
     return this.http.get(this._url + "alerts");
   }

   /**
    * Retrieve one alert from database with specific id
    * @param id 
    * GET method
    */
   getAlertById(id) {
     const url = this._url + "alerts/" + id;
     return this.http.get(url);
   }

   /**
    * Retrive all alert from database with specific status
    * @param status 
    * GET method
    */
   getAlertByStatus(status) {
     const url = this._url + "alerts/status/" + status;
     return this.http.get(url);
   }

   /**
    * Add an alert to database
    * @param data 
    * POST method
    */
   addAlert(data) {
     console.log(data);
     return this.http.post(this._url + "alerts", data);
   }

   /**
    * Update an alert from database
    * @param id 
    * @param data 
    * PATCH method
    */
   updateAlert(id, data) {
     const url = this._url + "alerts/" + id;
     return this.http.patch(url, data, httpOptions);
   }



}
