import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  /**
   * Base URL of API
   * You can find this on environment.ts 
   */
  private _url : string;

  constructor() { 
    this._url = environment.web_url;
  }

  ngOnInit() {
  }

}
