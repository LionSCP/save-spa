const mongoose = require('mongoose');

/**
 * Animal schema to model database
 */
const AnimalSchema = mongoose.Schema({
    type : {
        type: String,
        required: true
    }, 
});

module.exports = mongoose.model('Animal', AnimalSchema) 