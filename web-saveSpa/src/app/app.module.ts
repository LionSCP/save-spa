import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddAlertPageComponent } from './pages/add-alert-page/add-alert-page.component';
import { EditAlertPageComponent } from './pages/edit-alert-page/edit-alert-page.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AlertFormComponent } from './components/alert-form/alert-form.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { AlertsFilterComponent } from './components/alerts-filter/alerts-filter.component';
import { FilterPageComponent } from './pages/filter-page/filter-page.component';
import { MapPageComponent } from './pages/map-page/map-page.component';
import { MapComponent } from './components/map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    AddAlertPageComponent,
    EditAlertPageComponent,
    DashboardPageComponent,
    MenuComponent,
    FooterComponent,
    AlertFormComponent,
    EditFormComponent,
    AlertsFilterComponent,
    FilterPageComponent,
    MapPageComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
