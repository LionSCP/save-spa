import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-filter-page',
  templateUrl: './filter-page.component.html',
  styleUrls: ['./filter-page.component.scss']
})
export class FilterPageComponent implements OnInit {

  /**
   * Status of alert
   */
  private status: number;

  constructor(
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(
      params => this.status = params.status
    );
   }

  ngOnInit() {
  }

}
