import { Component, OnInit } from '@angular/core';
import { AlertsManagerService } from 'src/app/services/alerts-manager.service';
import { SquadManagerService } from 'src/app/services/squad-manager.service';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  /**
   * List of alerts from database
   */
  private alerts: Array<any>;

  /**
   * List of squads from database
   */
  private squads: Array<any>;

  constructor(
    private alertManager: AlertsManagerService,
    private squadManager: SquadManagerService,
    private router: Router,
    ) { 
      this.showAlerts();
      this.getAllSquads();
    }


  ngOnInit() {
  }

  /**
   * Form Control to assign a squad to alert
   */
  squad = new FormControl('');

  /**
   * Function to get all alerts and retrieve it on dashboard
   * From Alert Manager
   */
  showAlerts() {
    this.alertManager.getAlerts()
    .subscribe(
      (data: Array<any>) => {
        this.alerts = data;
        console.log(this.alerts);
      }),
      error => console.log(error);
  }

  /**
   * Function to get all squads from Squad Manager
   */
  getAllSquads() {
    this.squadManager.getSquads()
    .subscribe(
      (data: Array<any>) => {
        this.squads = data;
      }),
      error => console.log(error);
  }

  /**
   * Function to assign a squad to an alert and change status to Assigned
   * @param alertID 
   */
  assignSquad(alertID) {
    let data = {
      squad: this.squad.value,
      status: 1
    };
    this.alertManager.updateAlert(alertID, data)
    .subscribe(data => data,
      error => console.log(error));
    window.location.reload();
  }

  /**
   * Function to perform click toward Edit Page
   * @param alertID 
   */
  editAlert(alertID) {
    this.router.navigate(['/edit-alert/' + alertID]);
  }
}
