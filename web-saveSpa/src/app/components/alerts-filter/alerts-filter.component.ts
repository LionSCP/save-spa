import { Component, OnInit, Input } from '@angular/core';
import { AlertsManagerService } from 'src/app/services/alerts-manager.service';
import { SquadManagerService } from 'src/app/services/squad-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-alerts-filter',
  templateUrl: './alerts-filter.component.html',
  styleUrls: ['./alerts-filter.component.scss']
})
export class AlertsFilterComponent implements OnInit {

  @Input() status;
  private squads: any;
  private alerts: any;

  constructor(
    private alertManager: AlertsManagerService,
    private squadManager: SquadManagerService,
    private route: ActivatedRoute,
    private router: Router
    ) { 
      this.route.params.subscribe(
        params => this.status = params.status
      );
    }
    
    ngOnInit() {
      if (this.status) {
        this.getAlertsByStatus(this.status);
      } else {
        this.getAlertsByStatus(0);
      }
      this.getAllSquads();
    }

  /**
     * Form Control to assign a squad to alert
   */
  squad = new FormControl(['']);

  /**
   * Function to get all squads from Squad Manager
   */
  getAllSquads() {
    this.squadManager.getSquads()
    .subscribe(
      (data: Array<any>) => {
        this.squads = data;
      }),
      error => console.log(error);
    }

  getAlertsByStatus(status) {
    this.alertManager.getAlertByStatus(status)
    .subscribe((data) => {
      this.alerts = data;
    },
    error => console.log(error))
  }

  /**
   * Function to assign a squad to an alert and change status to Assigned
   * @param alertID 
   */
  assignSquad(alertID) {
    let data = {
      squad: this.squad.value,
      status: 1
    };
    this.alertManager.updateAlert(alertID, data)
    .subscribe(data => data,
      error => console.log(error));
    window.location.reload();
  }

  /**
   * Function to perform click toward Edit Page
   * @param alertID 
   */
  editAlert(alertID) {
    this.router.navigate(['/edit-alert/' + alertID]);
  }

  /**
   * Refresh page with new alerts
   * @param status 
   */
  refreshTable(status) {
    
    this.router.navigateByUrl('filter/' + status);
    window.location.reload();
  }


}
