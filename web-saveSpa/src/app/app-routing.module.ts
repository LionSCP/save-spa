import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { AddAlertPageComponent } from './pages/add-alert-page/add-alert-page.component';
import { EditAlertPageComponent } from './pages/edit-alert-page/edit-alert-page.component';
import { AlertFormComponent } from './components/alert-form/alert-form.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { AlertsFilterComponent } from './components/alerts-filter/alerts-filter.component';
import { FilterPageComponent } from './pages/filter-page/filter-page.component';
import { MapComponent } from './components/map/map.component';
import { MapPageComponent } from './pages/map-page/map-page.component';

const routes: Routes = [
  { path: 'component/menu', component: MenuComponent },
  { path: 'component/footer', component: FooterComponent},
  { path: 'component/alert-form', component: AlertFormComponent},
  { path: 'component/edit-form', component: EditFormComponent},
  { path: 'component/alerts-filter/:status', component: AlertsFilterComponent},
  { path: 'component/map', component: MapComponent},
  { path: 'dashboard', component: DashboardPageComponent},
  { path: 'save-an-animal', component: AddAlertPageComponent},
  { path: 'edit-alert/:id', component: EditAlertPageComponent},
  { path: 'filter/:status', component: FilterPageComponent},
  { path: 'map', component: MapPageComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
