/**
 * Express Configuration
 */
const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 3002;
const config = require('./config');

/**
 * Controllers
 */
const animalCtrl = require('./controllers/animalController');
const alertCtrl = require('./controllers/alertsController');
const squadCtrl = require('./controllers/squadsController');

const nominatim = require('./controllers/modules/nominatim');

const bodyParser = require('body-parser');

/**
 * Database configuration
 * mLab database at https://mlab.com/
 */
const mongoose = require('mongoose');
mongoose.connect(`mongodb://${config.db.user}:${config.db.password}@ds043158.mlab.com:43158/db_save-spa`, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, `Connection error: cannot connect to ${config.db.name}`));
db.once('open', () => {
    console.log(`Connected with succes to database : ${config.db.name}`);
});

app.use(cors());
const jsonParser = bodyParser.json();

//Routes
app.get('/', (req, res) => {
    res.send('Hello world !');
})

/**
 * Test of nominatim function
 */
app.get('/api/nominatim-test/:place', (req, res) => {
    const place = req.params.place;
    console.log(place)

    nominatim.getGeocode(place).then((data) => {
        res.send(data);
    })


})

/**
 * Routes
 */
app.get('/api/alerts', alertCtrl.getAlerts);
app.get('/api/alerts/:id', alertCtrl.getAlertById);
app.get('/api/alerts/status/:status', alertCtrl.getAlertByStatus);
app.post('/api/alerts', jsonParser, alertCtrl.addAlert);
app.patch('/api/alerts/:id', jsonParser, alertCtrl.updateAlert);

app.get('/api/animals', jsonParser, animalCtrl.getAnimals);
app.post('/api/animals', jsonParser, animalCtrl.addAnimal);

app.get('/api/squads', jsonParser, squadCtrl.getSquads);
app.post('/api/squads', jsonParser, squadCtrl.addSquad);

app.listen(PORT, () => {
    console.log(`Save-SPA app listening on port ${PORT} !`);
})
