import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnimalManagerService {

  /**
   * Base URL of API
   * You can find this on environment.ts 
   */
  private _url: string;

  constructor(
    private http: HttpClient
  ) { 
    this._url = environment.url;
  }

  /**
   * Retrieve all animals from database
   * GET method
   */
  getAnimals() {
    return this.http.get(this._url + "animals");
  }
}
