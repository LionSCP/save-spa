const Alert = require('../models/Alert');
const nodemailer = require('nodemailer');
const config = require('../config');

/**
 * Configuration of mailer
 */
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: config.email.id, 
        pass: config.email.password
    }
});

/**
 * Async function to get alerts from database
 * @param {*} req 
 * @param {*} res 
 */
const getAlerts = async (req, res) => {
    
    try {
        let alerts = await Alert.find()
        .populate('animal')
        .populate('squad')
        .exec();
        res.status(200).send(alerts);
    } catch (err) {
        console.error("Impossible to retrieve alerts");
        res.sendStatus(500);
    }
};

/**
 * Async function to get alerts by status
 * @param {*} req 
 * @param {*} res 
 */
const getAlertByStatus = async (req, res) => {
    
    const status = req.params.status;
    try {
        let alerts = await Alert.find({status: status})
        .populate('animal')
        .populate('squad')
        .exec()
        res.status(200).send(alerts);
    } catch (err) {
        console.error(`Impossible to retrieve alerts with status ${status}`);
        res.sendStatus(500);
    }
};

/**
 * Async function to get alert by ID
 * @param {*} req 
 * @param {*} res 
 */
const getAlertById = async (req, res) => {
    
    const id = req.params.id;
    try {
        let alert = await Alert.find({_id: id})
        .populate('animal')
        .populate('squad')
        .exec()
        res.status(200).send(alert);
    } catch (err) {
        console.error(`Impossible to retrieve alert with id n°${id}`);
        res.sendStatus(500);
    }
};

/**
 * Async funtion to add an alert to database and send email to Save SPA
 * @param {*} req 
 * @param {*} res 
 */
const addAlert = async (req, res) => {

    const body = req.body;
    const newAlert = new Alert(body);

    const to = config.email.id;
    const subject = 'Signalement d\'un animal abandonné';
    const text = `Un signalement vient d\'être effectué à ${body.adress.number} ${body.adress.street}, ${body.adress.zipcode} - ${body.adress.city}, par ${body.alertor}`;

    try {
        let alert = await newAlert.save();
        alert = await alert.generatePoint();
        res.status(201).send(alert);
    } catch (err) {
        console.error("An error was occured when try to add alert to database", err);
        res.sendStatus(500);
    }

    sendEmail(to, subject, text);
};

/**
 * Async function to update an alert and send email with status to alertor
 * @param {*} req 
 * @param {*} res 
 */
const updateAlert = async (req, res) => {

    const id = req.params.id;
    const body = req.body;

    
    try {
        let alert = await Alert.findByIdAndUpdate(id, {$set: body}, {new: true});
        res.status(200).send(alert);
        
        const to = alert.alertor;
        const subject = "Le statut de votre signalement a changé !";
        let text;
        switch (body.status) {
            case 1:
            text = `Le statut de votre signalement a changé en Assigné`;
            break;
            case 2:
            text = `Le statut de votre signalement a changé en Sauvé`;
            break;
            case 3:
            text = `Le statut de votre signalement a changé en Echec`;
            break;
            case 4:
            text = `Le statut de votre signalement a changé en Annulé`;
            break;
        }
        sendEmail(to, subject, text);
    } catch (err) {
        console.error("An error was occured when try to update this alert", err);
        res.sendStatus(500);
    }
}

/**
 * Function to send automatic email
 * @param {*} to 
 * @param {*} subject 
 * @param {*} text 
 */
const sendEmail = (to, subject, text) => {

    let mailOptions = {
        from: `"Save SPA" <${config.email.id}>`,
        to: to, 
        subject: subject, 
        text: text, 
    };

    nodemailer.createTestAccount((err) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (error) {
                return console.log(error);
            }
            console.log(info);
        });
    }); 
}


module.exports = {
    getAlerts,
    getAlertByStatus,
    addAlert,
    updateAlert,
    getAlertById
}