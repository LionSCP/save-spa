import { TestBed, inject } from '@angular/core/testing';

import { SquadManagerService } from './squad-manager.service';

describe('SquadManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SquadManagerService]
    });
  });

  it('should be created', inject([SquadManagerService], (service: SquadManagerService) => {
    expect(service).toBeTruthy();
  }));
});
